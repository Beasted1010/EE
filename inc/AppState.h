



#ifndef APPSTATE_H
#define APPSTATE_H

#include "Common.h"
#include "stdlib.h"

typedef enum StateEnum
{
    AppMainMenu,
    AppAbout,
    AppCalculate,
    AppRefresh,
    AppFreeze,
    AppTest,
    AppQuit
}State;

typedef struct ValuesStruct
{
    float numYears;
    float interestRate_Per;
    float PresentWorth;
    float FutureWorth;
    float AnnualWorth;
    float ArithmeticGradient_Per;
    float GeometricGradient_Per;
}Values;

typedef struct AppStateStruct
{
    int appState;
    Values values;
}AppState;




AppState* CreateApp( );
void DestroyApp( AppState* app );


#endif
