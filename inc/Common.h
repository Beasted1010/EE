
// Preprocessor stuff for warnings and such


#ifdef DEBUG
 #include "stdio.h"
 #define applog(...) printf(__VA_ARGS__); printf("...\n")
 #define applogerror(...) printf("Error > "); applog(__VA_ARGS__); printf("\n")
 #define applogwarning(...) printf("Warning > "); applog(__VA_ARGS__); printf("\n")
 #define applogline applog( "Logging line at: %d\n", __LINE__ )
 #else
 #define applog(...)
 #define applogerror(...)
 #define applogwarning(...)
 #define applogline
#endif
