


// TODO: Move function body to separate .c file


#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include "Functions.h"
#include "AppState.h"

#include <stdio.h>

//#############################################################################
//Helper Functions

/*//INPUT FUNCTIONS
//TODO: If the reuse of these is small, consider getting rid of 'em
static inline void GetPresentWorth_Ref(  AppState* app  )
{
    printf("What is the Present Worth?: ");
    scanf("%f", &app->values.PresentWorth);
}

static inline void GetFutureWorth_Ref( AppState* app )
{
    printf("What is the Future Worth?: ");
    scanf("%f", &app->values.FutureWorth);
}

static inline void GetNumYears_Ref(  AppState* app  )
{
    printf("How many years will pass?: ");
    scanf("%f", &app->values.numYears);
}

static inline void GetInterest_Ref(  AppState* app  )
{
    printf("What is the Interest Rate? (In Percent): ");
    scanf("%f", &app->values.interestRate_Per);
}

//COMMONLY USED FUNCTIONS
static inline void GetYearsAndInterest_Ref(  AppState* app  )
{
    GetNumYears_Ref( app );
    GetInterest_Ref( app );
}
*/

//OUTPUT FUNCTIONS

static inline void PrintFutureWorth( AppState* app )
{
    printf("The Future Worth is: %0.2f\n", app->values.FutureWorth);
}

static inline void PrintPresentWorth( AppState* app )
{
    printf("The Present Worth is: %0.2f\n", app->values.PresentWorth);
}

//END Helper Functions
//#############################################################################


void PrintHelpMenu()
{
    printf("P = Present Worth\n");
    printf("F = Future Worth\n");
    printf("i = Interest Rate (In Percent)\n");
    // TODO: Make sure n can be months, years and such
    printf("n = Number of Periods (Default = Years)\n");
    // TODO: Perhaps use this as the identifier for the above, this be a string then?
    printf("t = Time (Stated in Periods of Years, Months, Days)\n");
}

// TODO: Make this allow 'n/a' for unused instead of -1?
void GetAllValues( AppState* app )
{
    printf("Enter in the following values.\n");
    // NOTE: Good Idea? Bad Idea?
    printf("Enter in '0' if that value is unknown\n\n");

    //TODO: If this is in periods... need to ask for period unit? (i.e. years, months, ect)
    //Maybe specify COMPOUNDING/PAYMENT... periods??
    printf("How many periods will pass?: ");
    scanf("%f", &app->values.numYears);

    printf("What is the Interest Rate? (In Percent): ");
    scanf("%f", &app->values.interestRate_Per);

    printf("What is the Present Worth?: ");
    scanf("%f", &app->values.PresentWorth);

    printf("What is the Future Worth?: ");
    scanf("%f", &app->values.FutureWorth);

    printf("What is the Annual Worth?: ");
    scanf("%f", &app->values.AnnualWorth);

    //TODO: Make this more User Friendly
    printf("What is the Arithmetic Gradient?: ");
    scanf("%f", &app->values.ArithmeticGradient_Per);

    printf("What is the Geometric Gradient?: ");
    scanf("%f", &app->values.GeometricGradient_Per);
}

void PrintAllValues( AppState* app )
{
    printf("\nYour updated values are...\n");
    printf("The Number of Years is: %0.0f years\n", app->values.numYears);
    printf("The Interest Rate is: %0.0f\n", app->values.interestRate_Per);
    printf("The Present Worth is: $%0.2f\n", app->values.PresentWorth);
    printf("The Future Worth is: $%0.2f\n", app->values.FutureWorth);
    printf("The Annual Worth is: $%0.2f\n", app->values.AnnualWorth);
    printf("The Arithmetic Gradient is: %0.2f\n", app->values.ArithmeticGradient_Per);
    printf("The Geometric Gradient is: %0.0f\n", app->values.GeometricGradient_Per);
}


void PrintMainMenu()
{
    // Any invalid valid sends user to help
    printf("ENTER 'help' FOR HELP (Or any invalid input)\n");
    printf("1. Calculate Future Worth (Given: P, i, n)\n");
    printf("2. Calculate Present Worth (Given: F, i, n)\n");
    printf("3. Calculate Present Worth (Given: A, i, n)\n");
    printf("4. Calculate Annual Worth (Given: P, i, n)\n");
    printf("5. Calculate Future Worth (Given: A, i, n)\n");
    printf("6. Calculate Annual Worth (Given: F, i, n)\n");
    printf("7. Calculate Present Worth (Given: G, i, n)\n");
    printf("8. Calculate Annual Worth (Given: G, i, n)\n");
    printf("9. Calculate Present Worth (Given: A, g, i, n)\n");

    // TODO: Make this change only the selected text.
    //system("COLOR 0C");
    printf("Enter in the appropriate numeric Choice: ");
}

// NOTE: This is a bad idea... Maybe?
void MainMenuAction( AppState* app )
{
    //TODO: Ensure that else ifs are fine... any overlapping info?
    //any lost data?
    if( app->values.interestRate_Per == 0 )
    {

    }

    if( app->values.numYears == 0 )
    {

    }

    // Future Worth
    if( app->values.FutureWorth == 0 )
    {
        applog("Future Worth equals 0");
        if( app->values.PresentWorth != 0 )
        {
            applog("Calculating F/P");
            app->values.FutureWorth = FutureWorthGivenPresent( app->values.PresentWorth, app->values.numYears, app->values.interestRate_Per );
        }
        else if( app->values.AnnualWorth != 0 )
        {
            applog("Calculating F/A");
            app->values.FutureWorth = FutureWorthGivenAnnual( app->values.AnnualWorth, app->values.numYears, app->values.interestRate_Per );
        }
    }

    // Present Worth
    if( app->values.PresentWorth == 0 )
    {
        applog("Present Worth equals 0");
        if( app->values.FutureWorth != 0 )
        {
            applog("Calculating P/F");
            app->values.PresentWorth = PresentWorthGivenFuture( app->values.FutureWorth, app->values.numYears, app->values.interestRate_Per );
        }
        else if( app->values.AnnualWorth != 0 )
        {
            applog("Calculating P/A");
            app->values.PresentWorth = PresentWorthGivenAnnual( app->values.AnnualWorth, app->values.numYears, app->values.interestRate_Per );
        }
        else if( app->values.ArithmeticGradient_Per != 0 )
        {
            applog("Calculating P/G");
            app->values.PresentWorth = PresentWorthGivenGradient( app->values.ArithmeticGradient_Per, app->values.numYears, app->values.interestRate_Per);
        }
        if( app->values.GeometricGradient_Per != 0)
        {
            applog("Calculating P/Ag");
            app->values.PresentWorth = PresentWorthGeometricGradient( app->values.AnnualWorth,  app->values.GeometricGradient_Per, app->values.numYears, app->values.interestRate_Per );
        }
    }

    // Annual Worth
    if( app->values.AnnualWorth == 0 )
    {
        applog("Annual Worth equals 0");
        if( app->values.PresentWorth != 0 )
        {
            applog("Calculating A/P");
            app->values.AnnualWorth = AnnualWorthGivenPresent( app->values.PresentWorth, app->values.numYears, app->values.interestRate_Per );
        }
        else if( app->values.FutureWorth != 0 )
        {
            applog("Calculating A/F");
            app->values.AnnualWorth = AnnualWorthGivenFuture( app->values.FutureWorth, app->values.numYears, app->values.interestRate_Per );
        }
        if( app->values.ArithmeticGradient_Per != 0 )
        {
            applog("Calculating A/G");
            app->values.AnnualWorth = AnnualWorthGivenGradient( app->values.ArithmeticGradient_Per, app->values.numYears, app->values.interestRate_Per );
        }
    }

    // Arithmetic Gradient
    if( app->values.ArithmeticGradient_Per == 0 )
    {
        applog("Arithmetic Gradient equals 0");
    }

    // Geometric Gradient
    if( app->values.GeometricGradient_Per == 0 )
    {
        applog("Geometric Gradient equals 0");
    }

/*switch( choice )
    {
        case 1:
            DoFutureWorthGivenPresent( app );
            break;
        case 2:
            DoPresentWorthGivenFuture( app );
            break;
        default:
            PrintHelpMenu();
            app->appState = AppMainMenu;
            break;
    }*/
}


#endif
