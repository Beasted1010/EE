
#include "Functions.h"
#include "AppState.h"

#ifndef TEST_ENVIORNMENT_H
#define TEST_ENVIORNMENT_H

const float PW = 10000;
const float FW = 0;
const float AW = 0;
const float rateInPer = 8;
const float numYears = 20;
const float FaceValue = 50000;
const float dividend = -1;
const float couponRateInPer = 20;
const float numPaymentPeriods = 4;
const float maturityDate = 5;


//TODO: Have a way to verify these tests... Configuration file? Some other tool? Think VSU PASS/FAIL type stuff.

void RunFunctionTest()
{
    // (PW & numYears & rateInPer)
    // Correct Value (10,000 & 20 & 8): $46610
    printf( "Future Worth Given Present Worth Value: $%0.2f\n", FutureWorthGivenPresent( PW, numYears, rateInPer ) );

    // Correct Value (10,000 & 20 & 8): $35272.78
    printf( "Present Worth Bond Value: $%0.2f\n", PresentWorthOfBond( FaceValue, rateInPer, couponRateInPer, numPaymentPeriods, maturityDate, dividend) );
}

void RunBoundsTest()
{

}

void RunExtremesTest()
{

}

#endif
