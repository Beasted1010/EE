
#ifndef FUNCTIONS_H
#define FUNCTIONS_H



// Move to own "MATH" file?
//#############################################################################
//Math Tools

//CONSTANTS
const float PI = 3.1415926535f;
const float e = 2.718281828f;

inline static float Exponentiate( float base, float exponent )
{
    int i;
    float result = 1;
    for( i = 0; i < exponent; i++ )
    { result *= base; }

    return result;
}

inline static float DecimalExponentExponentiate( float base, float exponent )
{
    float result = 1;
    // TODO: Need to figure out way to calculate an irrational exponent
    return result;
}

inline static float Square( float base )
{
    return Exponentiate( base, 2 );
}

inline static float PerToDec( float rateInPer )
{
    return rateInPer / 100;
}

inline static float WholePlusRate( float rateInDec )
{
    return 1 + rateInDec;
}

// Does both PerToDec and WholePlusRate in one
// Returns the Whole Rate version given a rateInPer
inline static float PerToDec_WholeRate( float rateInPer )
{
    return WholePlusRate( PerToDec( rateInPer ) );
}

//END Math Tools
//#############################################################################





//#############################################################################
//Factors

//SINGLE AMOUNT
// Find F (Future Worth) given P (Present Worth)
float FP_Factor( float numYears, float rateInPer )
{
    return Exponentiate( PerToDec_WholeRate( rateInPer ), numYears );
}

// Find P (Present Worth) given F (Future Worth)
float PF_Factor( float numYears, float rateInPer )
{
    return ( 1 / FP_Factor( numYears, rateInPer ) );
}

//UNIFORM SERIES
// Find P (Present Worth) given A (Annual Worth)
float PA_Factor( float numYears, float rateInPer )
{
    float whole_rate = PerToDec_WholeRate( rateInPer );
    float numerator = Exponentiate( whole_rate, numYears ) - 1;
    float denominator = PerToDec( rateInPer ) * Exponentiate( whole_rate, numYears );

    return ( numerator / denominator );
}

// Find A (Annual Worth) given P (Present Worth)
float AP_Factor( float numYears, float rateInPer )
{
    return ( 1 / PA_Factor( numYears, rateInPer ) );
}

// Find F (Future Worth) given A (Annual Worth)
float FA_Factor( float numYears, float rateInPer )
{
    float whole_rate = PerToDec_WholeRate( rateInPer );
    float numerator = Exponentiate( whole_rate, numYears ) - 1;
	
    return ( numerator / PerToDec( rateInPer ) );
}

// Find A (Annual Worth) given F (Future Worth)
float AF_Factor( float numYears, float rateInPer )
{
    return ( 1 / FA_Factor( numYears, rateInPer) );
}

//ARITHMETIC GRADIENT
// Find Pg (Present Worth of gradient) given G (Arithmetic Gradient)
//TODO: should these be PgG_Factor and AgG_Factor???
float PG_Factor( float numYears, float rateInPer )
{
    float whole_rate_exp = Exponentiate( PerToDec_WholeRate( rateInPer ), numYears );
    float Per_Dec = PerToDec( rateInPer );
    float numerator = whole_rate_exp - ( Per_Dec * numYears ) - 1;
    float denominator = Square( Per_Dec ) * whole_rate_exp;

    return ( numerator / denominator );
}

// Find Ag (Annual Worth of gradient) given G (Arithmetic Gradient)
float AG_Factor( float numYears, float rateInPer )
{
    float Per_Dec = PerToDec( rateInPer );
    float minuend = ( 1 / Per_Dec );
    float subtrahend = ( numYears ) / ( Exponentiate( PerToDec_WholeRate( rateInPer ), numYears ) - 1 );

    return ( minuend - subtrahend );
}

//GEOMETRIC GRADIENT
// Find Pg (Present Worth) given A1 (Annual Worth) and g (Ratio/gradient)
// TODO: FLAG!! THIS MAY FAIL DUE TO ASSUMPTIONS WITH g AND i
float PAg_Factor( float gradientInPer, float numYears, float rateInPer )
{
    // 1 + g_inPer
    float g = PerToDec_WholeRate( gradientInPer );
    // 1 + i_inPer
    float i = PerToDec_WholeRate( rateInPer );


    if( g != i )
    {
        float ratio = g / i;
        return ( 1 - Exponentiate( ratio, numYears ) ) / ( i - g );
    }
    else
    {
        return ( numYears / i );
    }
}


//END Factors
//#############################################################################





//#############################################################################
//Calculations

/***********************************************************************************/
//TIME VALUE OF MONEY CALCULATIONS
//SINGLE AMOUNT
float FutureWorthGivenPresent( float PresentWorth, float numYears, float rateInPer )
{
    return (PresentWorth * FP_Factor( numYears, rateInPer ) );
}

float PresentWorthGivenFuture( float FutureWorth, float numYears, float rateInPer )
{
    return ( FutureWorth * PF_Factor( numYears, rateInPer ) );
}

//UNIFORM SERIES
float PresentWorthGivenAnnual( float AnnualWorth, float numYears, float rateInPer )
{
    return ( AnnualWorth * PA_Factor( numYears, rateInPer ) );
}

float AnnualWorthGivenPresent( float PresentWorth, float numYears, float rateInPer )
{
    return ( PresentWorth * AP_Factor( numYears, rateInPer ) );
}

float FutureWorthGivenAnnual( float AnnualWorth, float numYears, float rateInPer )
{
    return ( AnnualWorth * FA_Factor( numYears, rateInPer ) );
}

float AnnualWorthGivenFuture( float FutureWorth, float numYears, float rateInPer )
{
    return ( FutureWorth * AF_Factor( numYears, rateInPer ) );
}

//ARITHMETIC GRADIENT
float PresentWorthGivenGradient( float Gradient, float numYears, float rateInPer )
{
    return ( Gradient * PG_Factor( numYears, rateInPer ) );
}

float AnnualWorthGivenGradient( float Gradient, float numYears, float rateInPer )
{
    return ( Gradient * AG_Factor( numYears, rateInPer ) );
}

//GEOMETRIC GRADIENT
float PresentWorthGeometricGradient( float AnnualWorth, float gradientInPer, float numYears, float rateInPer )
{
    return ( AnnualWorth * PAg_Factor( gradientInPer, numYears, rateInPer ) );
}
/***********************************************************************************/


/***********************************************************************************/
//BOND CALCULATIONS
//PRESENT WORTH OF BOND
float PresentWorthOfBond( float FaceValue, float rateInPer, float couponRateInPer, float numPaymentPeriods,
                                                                        float maturityDate, float dividend )
{
    float compoundingPeriods = numPaymentPeriods * maturityDate;

    if( dividend < 0 )
    {
        dividend = ( FaceValue * PerToDec( couponRateInPer ) ) / numPaymentPeriods;
    }

    return PresentWorthGivenAnnual( dividend, compoundingPeriods, rateInPer )
                                    + PresentWorthGivenFuture( FaceValue, compoundingPeriods, rateInPer);
}
/***********************************************************************************/


/***********************************************************************************/
//INTEREST CALCULATIONS
//EFFECTIVE INTEREST
//TODO: Make the parameters strings? Allowing for "Year" or "Month" as answers to interestPeriod, or compoundingPeriod??
//Right now it reads as "How many interestPeriods? -> 4, oh okay, 4 interest periods..." but what unit is interest periods??
//Perhaps just ask for the values in units of months...?
float CalculateInfiniteCompoundingEffectiveInterest( float nominalRateInPer, float interestPeriods, float paymentPeriods )
{
    // NOTE: ATM this function call does not do anything, need to figure out way to calculate decimal exponent...
    return ( DecimalExponentExponentiate( e, ( PerToDec( nominalRateInPer ) / interestPeriods / paymentPeriods) ) - 1 );
}

float CalculateFiniteCompoundingEffectiveInterest( float nominalRateInPer, float interestPeriods, float compoundingPeriods, float paymentPeriods )
{
    return ( DecimalExponentExponentiate( ( 1 + ( PerToDec( nominalRateInPer ) / (interestPeriods / compoundingPeriods) ) ),
                                                                              (paymentPeriods / compoundingPeriods) ) - 1 );
}

//INFLATED INTEREST
float CalculateInflatedInterest( float realRateInPer, float inflationRateInPer )
{
    float realRate = PerToDec( realRateInPer );
    float inflationRate = PerToDec( inflationRateInPer );
    return ( realRate + inflationRate + (realRate * inflationRate) );
}

//REAL INTEREST
float CalculateRealInterest( float inflationRateInPer, float inflatedRealRateInPer )
{
    float inflationRate = PerToDec( inflationRateInPer );
    return ( ( PerToDec( inflatedRealRateInPer ) - inflationRate ) / (1 + inflationRate) );
}

/***********************************************************************************/


//END Calculations
//#############################################################################


#endif
