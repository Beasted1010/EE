/*
Ideas:
Allow user to set color of text for particular output...? For Windows Application?
Allow for saved values (with configuration file)
Allow for user to "export to 'excel'"
Have tests for each factor, to be compared to the tabulated values
    -Perhaps allow for option to create the entire table itself
Determine if float/int/double ect are most efficient for the values expected
Calculate Effective Interest Rates formulas.
    - Allow user to compare quotes with varying interest rates and time periods
Graphs and Simulations

NOTE:
Nominal interest rate must be converted to effective interest rate before calculations.
    - Effective interest rate MUST be used in any calulations involving formulas, tables, ect


TODO:
Make all years UINT8? Safe to assume 100+ years is unrealistic?
Move Functions.h stuff to an appropriate Functions.c
Have error handling cases, if value is too large, ect.
    - Use the applogerror/warning/line ect in these cases
Need some sort of "AppState" to keep track of where we are in the app
    - To be used in resetting user back to main menu
Gradients and such are becoming a pain. Ensure these are involved properly for calculations
Allow user to reenter a value (say if they entered in an incorrect value)
Allow for user to save values to be used later.
    -And a retrieval mechanism so user may retrieve value at any time
Highlight the text of values that have changed (red?) and values that have not changed (green?) this may contradict the idea of color coding high/low colors.
Clean up function variables, if it is not neccessary to have multiple variables (i.e. can just have one return statement with entire calculation), rid em.
    - Perhaps an exception would be for readability in the future, in case I desire to educate the user on functions being used, would be nice for easy readability to use in explanation.
    - If multiple function calls would be needed without the use of another variable, then use another variable.

TODO: Considerations

JUST HAVE ENTRY FIELDS THAT AUTO UPDATE GIVEN APPROPRIATE VALUES...
    -Oh you entered in i, n and p? ok I will update the F entry field
    -Maybe have a checkbox to indicate the values are not to be updated?
        -To remain constant
A separate text color indicating severity of value (Low, High, Growth...)
For testing enviornment, have some sort of verification scheme (and something to test against), think VSU PASS/FAIL stuff

*/
