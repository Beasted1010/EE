

#include "AppState.h"

AppState* CreateApp( )
{
    AppState* app;
    app = malloc( sizeof(AppState) );

    //Initialization of all values
    app->values.interestRate_Per = 0.0f;
    app->values.numYears = 0.0f;
    app->values.PresentWorth = 0.0f;
    app->values.FutureWorth = 0.0f;
    app->values.AnnualWorth = 0.0f;
    app->values.ArithmeticGradient_Per = 0.0f;
    app->values.GeometricGradient_Per = 0.0f;

    // Start User off on Main Menu
    app->appState = AppMainMenu;

    applog("Created an app");
    return app;
}


void DestroyApp( AppState* app )
{
    free( app );
    applog("Destroyed an app");
}
