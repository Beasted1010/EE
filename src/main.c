#include "Functions.h"
#include "UserInterface.h"
#include "AppState.h"
#include "test_enviornment.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <windows.h>

//#pragma warning(disable : 4996) // _CRT_SECURE_NO_WARNINGS -> For Visual Studio

// Internal app name, with no spaces. Used for internal naming.
#define APPNAME_INTERNAL "FinancialCalculator"
// External app name, with spaces. Used for external naming.
#define APPNAME_EXTERNAL "Financial Calculator"

// TODO: These global variables are temporary
// Making these static means that they are intialized to 0 and local to only this translation unit/file
static BITMAPINFO bitmapInfo;
static void *bitmapMemory;
static int bitmapWidth;
static int bitmapHeight;

static int running;

// A char is 8 bits, below is commented out for clarity
// uint8_t is the same thing basically, but cross platform friendly
//typedef unsigned char uint8;
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

static void RenderWeirdGradient( int xOffSet, int yOffSet )
{
    /*
                                                       Width amount ->
                                                       0         width * bytesPerPixel
        bitmapMemory (pointer to our bitmap memory) -> BB GG RR xx BB GG RR xx ...
        That is one row.        width * bytesPerPixel is a pointer to the next row.


        bitmapMemory -> row 0    BB GG RR xx BB GG RR xx BB GG RR xx BB GG RR xx ...
        BMmem + pitch-> row 1    BB GG RR xx BB GG RR xx BB GG RR xx BB GG RR xx ...
        (BMmem + pitch) + pitch ... -> row 2
        or BMmem += pitch; -> will always point to the next row.
        NOTE: row is defined as bitmapMemory, and pixel is defined as beginning of row
              therefore pixel points to the begining of row, which gets incremented
              a lil for each new xRGB value before finishing with each pixel and finally
              row gets incremented by 'pitch', pixel would then get updated to row next
              iteration, ensuring that pixel always starts itration by pointing to first in row
        NOTE: We could of set row = (uint8*) pixel, because pixel is pointing to the next row
              after the rows iteration already (since we increment over bitmapWidth), when the
              inner loop finishes pixel will already be pointing to the next row appropriately
        NOTE: The added 8 bits for pad (the xx) can be used as an Alpha channel. The Alpha
              channel is a process of combining an image with a background to create the
              appearance of partial or full transparency when rendering an image. This is
              often useful to render image elements in separate passes, and then combine the
              resulting multiple 2D images into a single final image called the composite.
              This perhaps is what Jesse did for the stacked sandwich in Sandwich Boogaloo.
              Think Transparency/Opaque for SDL Rendering
              Would look like... AA RR GG BB
    */


    int bytesPerPixel = 4;
    // bitmapMemory is a void*, which C does not understand
    // We will need to change it to a pointer C does understands
    // Casting to an 8 bit unsigned integer because we will be doing pointer arithmetic
    // bitmapMemory points to the first byte in our bitmap, so row is also pointing there
    uint8* row = (uint8*)bitmapMemory;

    // Pitch is the difference between one row and the next row.
    // More precisely pitch is the amount we wish to go across for a row of pixels
    // So pitch is the width of our bitmap times the bytesPerPixel
    // Pitch is equal to the total # of bytes in a row of our bitmap
    int pitch = bitmapWidth * bytesPerPixel;

    // For each individual Row we are going to go over each pixel in that row
    for( int y = 0; y < bitmapHeight; ++y )
    {
        // Casting row pointer to a 1 byte integer
        //uint8* pixel = (uint8*)row;
        // Now setting it to a 32 byte pixel so we don't have to do 4 separate pixel adjustments
        // such as: *pixel = B; ++pixel; *pixel = G; ++pixel; *pixel = R; ++pixel; *pixel = 0;..
        uint32* pixel = (uint32*)row;
        for( int x = 0; x < bitmapWidth; ++x )
        {
            /*
                00 -> hexadecimal, 8 bits        (pixel + # bytes in)
                                  0   1   2   3 -> # bytes in from pointer
                Pixel in memory: 00  00  00  00
                WRONG ->         RR  GG  BB  xx -> Because little endian arch.
                Because this is LITTLE ENDIAN ARCHITECTURE
                which means when bytes are loaded out of memory to form a
                bigger value, (wanted to load 4 bytes and produce a uint32
                out of them, the processor will put the first byte in the lowest
                part of those 32 bits, the second byte will go in the next part
                and the next part... and the highest will be the 4th byte over)
                So basically what happens is if RR GG BB xx is the pixels in the
                memory, if you were to load them into uint32 you would load them
                R first into the low... 0x xxBBGGRR, now its backwards...
                So the Windows' devs wanted it to look like it was in order
                So they defined the memory order to be backwards
                So Pixels in memory IS ACTUALLY...
                Pixel in memory: BB GG RR xx
            */

            // Eliminating old way of doing it, one color at a time (8 bits each set)
            // Now going to do it in a more compact format

            // The colors we are using
            uint8 blue = (x + xOffSet);
            uint8 green = (y + yOffSet);

            /*
                Recall memory looks like this -> Memory: BB GG RR xx
                But it is stored in register as -> Register: xx RR GG BB
                So Blue is the bottom 8 bits
                In this example we are leaving xx and RR as 0

                Pixel (32-Bits)
            */

            // Move green over 8 bits where it belongs, and then tack blue on the bottom 8 bits
            // So that blue is the bottom 8 bits, and green is the next 8 bits, xx and RR are 0
            *pixel++ = ( (green << 8) | blue );
        }

        // We are done with an entire row, so now add the # of bytes we moved over
        // Recall that pitch is the # of bytes in a row of our bitmap
        // So row will now point to the next row of bytes
        row += pitch;
    }
}

// Will resize the DIB section, or initialize it if it has never been before
// Every time we get a WM_SIZE message we will basically call this function
static void ResizeDIBSection( int width, int height )
{
    // TODO: Maybe don't free first, free after, then free first if that fails
    if(bitmapMemory)
    {
        // 0 because we don't need to store the actual size (Windows does that for us)
        // MEM_RELEASE: We are giving memory BACK to operating system for it to do with as it wishes
        // MEM_DECOMMIT: Decommits the memory but saves the memory for later use, basically says leave the pages reserved. But don't bother tracking them
        VirtualFree(bitmapMemory, 0, MEM_RELEASE);
    }

    bitmapWidth = width;
    bitmapHeight = height;

    // Contains several options, see "BITMAPINFOHEADER structure" on MSDN
    bitmapInfo.bmiHeader.biSize = sizeof(bitmapInfo.bmiHeader);
    bitmapInfo.bmiHeader.biWidth = bitmapWidth;
    // neg. because by default, the bitmap is a bottom up DIB
    // neg. makes it a top down DIB, rows will now go sequentially top down
    // neg. also makes it agree with how pixels are numbered (0 is top left)
    bitmapInfo.bmiHeader.biHeight = -bitmapHeight;
    bitmapInfo.bmiHeader.biPlanes = 1;
    // We need 24 (8 bits for Red, 8 for Green, 8 for Blue...)
    // But asking for 32 because we want things to be "DWORD alligned"
    // Meaning that our pixels are aligned on 4 byte boundaries
    bitmapInfo.bmiHeader.biBitCount = 32;
    // Don't want any compression, want blitting done as fast as posible
    bitmapInfo.bmiHeader.biCompression = BI_RGB;
    // NOTE: Since bitmapInfo is declared static, all the below is set to 0 for free, so this is unnecessary
    // Needed if you have a compression format
    bitmapInfo.bmiHeader.biSizeImage = 0;
    // Only needed if you are using some kind of print output
    bitmapInfo.bmiHeader.biXPelsPerMeter = 0;
    bitmapInfo.bmiHeader.biYPelsPerMeter = 0;
    // How many colors are used in the color table... We do not use a color table
    bitmapInfo.bmiHeader.biClrUsed = 0;
    // We do not have any index palette stuff, so set to 0
    bitmapInfo.bmiHeader.biClrImportant = 0;

    // Found out that we did not need a DC, see stream Handmade Hero 004

    // We have a pixel being 32 bits, 4 BYTES (as we defined in biBitCount)
    // And we need width times the height worth of pixels..
    int bytesPerPixel = 4;
    // This is the size of our bitmap memory
    int bitmapMemorySize = (bitmapWidth * bitmapHeight) * bytesPerPixel;
    // bitmapMemory is equal to something that allocates bitmapMemorySize worth
    // 1st Param: We don't actually care where the memory is (so set to 0/NULL)
    // 2nd Param: Size of the region in bytes. Since Address Param is NULL the value is rounded up to the next page boundary.
    // 3rd Param: Allocation type, tells Windows what we actually want it to do to this memory range.
    // MEM_COMMIT: Actually start tracking the memory, we will actually start using the  memory (WE WANT THE MEMORY NOW)
    // MEM_RESERVE: If we just want to make sure memory is available for us when we need it but for it to not actually starting to track the memory yet. Wait till we need it.
    // 4th Param: Protection: What we want our access privaleges to be, for security/forking purposes)
    // Returns a LPVOID (void*)
    // This is the magic, we are now grabbing memory for our bitmap
    bitmapMemory = VirtualAlloc(0, bitmapMemorySize, MEM_COMMIT, PAGE_READWRITE);

    RenderWeirdGradient( 0, 0 );
}


//
static void GoUpdateWindow( HDC deviceContext, RECT *windowRect,
                            int x, int y, int width, int height )
{
    int windowWidth = windowRect->right - windowRect->left;
    int windowHeight = windowRect->bottom - windowRect->top;

    // A rectangle to rectangle copy
    // Destination will be 0,0 and full width and height (IF Full screen?)
    StretchDIBits( deviceContext,
                   x, y, bitmapWidth, bitmapHeight,
                   x, y, windowWidth, windowHeight,
                   bitmapMemory,
                   &bitmapInfo,
                   DIB_RGB_COLORS, SRCCOPY );
}






//
LRESULT CALLBACK MainWindowCallback( HWND hWnd, UINT message,
                             WPARAM wParam, LPARAM lParam )
{
    // result is the return value for this function, 0 represents that the message was handled.
    LRESULT result = 0;

    // Switch statement to determine which message is being sent in
    switch(message)
    {
        // Sent after window size has changed
        // Basically we want to draw each time we get the WM_SIZE message
        case WM_SIZE:
        {
            RECT clientRect;
            // LEFT and TOP will be 0, RIGHT and BOTTOM will be Width and Height
            GetClientRect( hWnd, &clientRect );
            int width = clientRect.right - clientRect.left; // WIDTH
            int height = clientRect.bottom - clientRect.top; // HEIGHT
            ResizeDIBSection( width, height );
            applog("WM_SIZE adjustment occured");
        } break;
        // Sent when a window belonging to a different application is activated
        case WM_ACTIVATEAPP:
        {
            applog("WM_ACTIVATEAPP event occured");
        } break;
        case WM_SETCURSOR:
        {
            // TODO: Create a cursor
        } break;
        // Sent when the "X" is pressed or "Close" is pressed
        case WM_CLOSE:
        {
            // TODO: Handle this with a message to the user? "Are you sure?"
            PostQuitMessage(0);
            applog("WM_CLOSE event occured");
        } break;
        // WM_DESTROY is sent after window is destroyed
        // This is not the way we wish to close the app, WM_CLOSE is the preferred method of closing the app
        case WM_DESTROY:
        {
            // TODO: Handle the WM_DESTROY event as an error?
            PostQuitMessage(0);
            applog("WM_DESTROY event occured");
            applogerror("App crashed unexpectedly");
        } break;
        // Sent when the system or another application makes a request to paint
        case WM_PAINT:
        {
            // The paint struct to be filled out by BeginPaint
            PAINTSTRUCT Paint;
            // Begin Paint fills out paint struct and prepares painting
            HDC deviceContext = BeginPaint(hWnd, &Paint);
            // rcPaint is the RECT we will be drawing in
            int x = Paint.rcPaint.left;
            int y = Paint.rcPaint.top;
            int width = Paint.rcPaint.right - Paint.rcPaint.left; // WIDTH
            int height = Paint.rcPaint.bottom - Paint.rcPaint.top; // HEIGHT

            RECT clientRect;
            GetClientRect( hWnd, &clientRect );

            GoUpdateWindow( deviceContext, &clientRect, x, y, width, height );
            // Tells Windows the painting event is over
            EndPaint(hWnd, &Paint);
            applog("WM_PAINT event occured");
        } break;
        // What happens when the message received is none of the above
        default:
        {
            //applog("DEFAULT");
            // The result of calling Windows' default window CALLBACK (DefWindowProc), a catch all function that basically handles any messages we don't wish to in their default way.
            result = DefWindowProc(hWnd, message, wParam, lParam);
        } break;
    }

    return result;
}



// hInstance: A value to identify the EXE when it is loaded into memory
// hPrevInstance: NO MEANING, USED IN 16 bit Windows, now always = 0
// pCmdLine: Contains command line arguments as a Unicode string
// nCmdShow: A flag to say whether the main application window will be minimized, maximized or shown normally
int CALLBACK WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
                                     PSTR lpCmdLine, int nCmdShow )
{
    //MessageBoxA(0, "Hello World!", "Title", MB_OK | MB_ICONINFORMATION);


    /* WNDCLASS (struct) members
    UINT      style; -> Defines additional elements to the window class
    WNDPROC   lpfnWndProc; -> A pointer to the new window procedure (A function)
    int       cbClsExtra; -> The number of extra bytes to allocate following the window-class structure (A buffer, default=0). Allows you to store extra bytes (your personal byte) to be used as you wish.
    int       cbWndExtra; -> The number of extra bytes to allocate following the window-class instance (A buffer, default=0) Allows you to store extra bytes (your personal byte) to be used as you wish.
    HINSTANCE hInstance; -> A handle to the instance that contains the window procedure for the class (similar to WinMain arg)
    HICON     hIcon; -> A handle to the class icon. Must be a handle to an icon resource. (NULL = Windows Provides a default)
    HCURSOR   hCursor; -> A handle to the class cursor. Must be a handle to a cursor resource. (NULL = App must explicitly set the cursor shape whenever the mouse moves into the application's window)
    HBRUSH    hbrBackground; -> A handle to the class background brush. Can be a handle to the physical brush used for painting the background, or a color value.
    LPCTSTR   lpszMenuName; -> The resource name of the class menu, as the name appears in the resource file. (Top menu strip)
    LPCTSTR   lpszClassName; -> A string that specifies the window class name
    */

    // Create a window class (a structure instance), initialize all members to 0
	WNDCLASS WindowClass = {0,0,0,0,0,0,0,0,0,0};

    // DC = Device Context -> Something Windows uses to keep the state of drawing while we are trying to interface with it to draw our window.
    // CS_OWNDC: Provides a unique DC for each window in the class
    // CS_HREDRAW: Redraws entire window if movement or size adjustment changes width of the client area.
    // CS_VREDRAW: Redraws entire window like HREDRAW, but for height
    WindowClass.style = CS_OWNDC|CS_HREDRAW|CS_VREDRAW;
    WindowClass.lpfnWndProc = MainWindowCallback;

    /* Note if you do not know hInstance or did not save it for use somewhere,
    you can use "GetModuleHandle(LPCTSTR lpModuleName)" to retrieve a module handle for the specified module.
    Must have been loaded by the calling process.
    GetModuleHandle(0) -> Gets the instance handle
    */
    WindowClass.hInstance = hInstance;
    //WindowClass.hIcon =
    //WindowClass.hCursor =
    // We will create our own renderer
    //WindowClass.hbrBackground =

    // Workaround to allow for easy name update that is convertable to LPSTR
    char str_array[30] = "";
    strcat(str_array, APPNAME_INTERNAL);
    WindowClass.lpszMenuName = strcat(str_array, "Menu");
    WindowClass.lpszClassName = strcat(str_array, "Class");

    // If fails, return value is 0
    if( RegisterClass(&WindowClass) )
    {
        // Create a new instance of a window
        // Param1: Optional window styles (i.e. Transparent. 0 is default)
        // Param2: Window class
        // Param3: Window text
        // Param4: Window style
        // Returns a handle to the new window, or 0 if fails
        HWND hAppMainWindow = CreateWindowExA(
            0, WindowClass.lpszClassName, APPNAME_EXTERNAL,
            WS_OVERLAPPEDWINDOW|WS_VISIBLE,
            // Size and position, CW_USEDEFAULT opens window anywhere Windows wishes, will wish to change later?
            CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
            NULL, // Parent window
            NULL, // Menu for the application window (if desired)
            hInstance, // Instance handle (handle to app instance, EXE)
            NULL // Additional application data
        );
        if(hAppMainWindow) // Window Successfuly created
        {
            MSG message; // A message struct
            // Param1: The place to put th emessage taht we will be retrieving
            // Param2: NULL will allow us to retrieve messages for any unbound windows. Will retrieve any messages for windows that belong to us. Grabs all messages that are coming in
            // Param3: Lowest message value to be retrieved
            // Param4: Highest message value to be retrieved
            // If Param3 & Param4 = 0 then we retrieve all messages
            // Will return a nonzero value until WM_QUIT is retrieved
            // Return value can be -1 if an error has occured
            // Windows will wait for a message, it will block and do other things
            // If we wish to use the CPU time all to ourselves we will need to use...
            // PeakMessage(...) which does the same thing but when there is no message it will
            // allow us to keep running. TODO: Consider if we really want PeakMessage(...)
            int xOffSet = 0;
            int yOffSet = 0;
            running = 1;
            while(running)
            {
                while( PeekMessage( &message, NULL, 0, 0, PM_REMOVE ) )
                {
                    if( message.message == WM_QUIT )
                    {
                        running = 0;
                    }
                    // Takes a message that came in off the message queue
                    // Does some processing to get it ready to be sent out
                    TranslateMessage(&message);
                    // Sends message out
                    DispatchMessageA(&message);
                }

                RenderWeirdGradient( xOffSet, yOffSet );

				HDC deviceContext = GetDC(hAppMainWindow);
                RECT clientRect;
                GetClientRect(hAppMainWindow, &clientRect);
                int windowWidth = clientRect.right - clientRect.left; // WIDTH
                int windowHeight = clientRect.bottom - clientRect.top; // HEIGHT
                GoUpdateWindow(deviceContext, &clientRect, 0, 0, windowWidth, windowHeight);
                ReleaseDC(hAppMainWindow, deviceContext);

                ++xOffSet;
                //++yOffSet;
        }
        }
        else
        {
            applogerror("Failed to receive handle back for AppMainWindow");
        }
    }
    else
    {
        applogerror("Failed to register class");
    }
    return 0;
}

/*
int main()
{
    printf("Welcome to your Money calculator!\n");
    printf("Version 0.05\n\n");

    AppState* app = CreateApp();
    int appRunning = 1;

    // AppLoop
    while( appRunning )
    {
        switch( app->appState )
        {
            case AppRefresh:
                DestroyApp( app );
                app = CreateApp( );
                applog("App refreshed");
                break;
            case AppMainMenu:
                //this prints out a ton... grr?
                applog("\nApp on MainMenu");
                do
                {
                    printf("Where to navigate?\n");
                    printf("1. AppMainMenu\n");
                    printf("2. AppAbout\n");
                    printf("3. AppCalculate\n");
                    printf("4. AppRefresh\n");
                    printf("5. AppTest\n");
                    printf("6. AppQuit\n");

                    printf("Choice: ");
                    int choice;
                    scanf("%d", &choice);
                    app->appState = AppFreeze;

                    if( choice == 1 )
                    { app->appState = AppMainMenu; }
                    else if( choice == 2 )
                    { app->appState = AppAbout; }
                    else if( choice == 3 )
                    { app->appState = AppCalculate; }
                    else if ( choice == 4 )
                    { app->appState = AppRefresh; }
                    else if (choice == 5 )
                    { app->appState = AppTest; }
                    else if ( choice == 6 )
                    { app->appState = AppQuit; }
                }while( app->appState == AppFreeze );
                break;
            case AppAbout:
                applog("App on About");
                app->appState = AppMainMenu;
                break;
            case AppCalculate:
                applog("App calculating");
                GetAllValues( app );
                MainMenuAction( app );
                PrintAllValues( app );
                app->appState = AppMainMenu;
                break;
            case AppTest:
                applog("App running tests");
                RunFunctionTest();
                RunBoundsTest();
                RunExtremesTest();
                app->appState = AppMainMenu;
                break;
            case AppQuit:
                appRunning = 0;
                applog("App preparing to close");
                break;
        }
    }

    applog("App closing");

    return 0;
}*/
